// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC721/extensions/ERC721Enumerable.sol";

contract HouseNFT is ERC721Enumerable {
    struct House {
        uint256 id;
        address owner;
        uint256 cost;
    }

    uint256 public cost;
    uint256 public maxSupply;

    House[] public houses;

    constructor(uint256 _cost, uint256 _maxSupply) ERC721("HouseNFT", "HOUSE") {
        cost = _cost;
        maxSupply = _maxSupply;
    }

    function mint() public payable {
        require(totalSupply() < maxSupply, "Maximum supply reached");
        require(msg.value >= cost, "Insufficient ETH sent");

        uint256 newHouseId = totalSupply();
        House memory newHouse = House(newHouseId, msg.sender, cost);
        houses.push(newHouse);

        _mint(msg.sender, newHouseId);
    }
}
