const HouseNFT = artifacts.require("HouseNFT");

module.exports = function (deployer) {
  const cost = 0.00001; // Coût d'une maison (en ETH)
  const maxSupply = 100; // Quantité totale de maisons disponibles

  deployer.deploy(HouseNFT, web3.utils.toWei(cost.toString(), 'ether'), maxSupply);
};
