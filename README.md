# React Truffle Box

Le script de migration se trouve dans le dossier migrations et le contract dans le dossier contracts.

## Préparation

Renommer le fichier exemple.env en .env. Esuite lui mettre votre path phrase et l'id du projet Mubai.

## Installation

Pour l'installation sous linux, faire, au niveau de la racine du projet:

sh installLinux.sh

Pour Windows faire ces commandes les une après les autres:

npm install 
cd client 
npm install 
cd ..
npm run migrate 

## Execution

Pour Linux:

sh start.sh

Pour windows rappartir de la racine:

cd client
npm start

Si besoin, contacté le groupe pour de l'aide.