import React from "react";
import "./App.css";
import {useEffect, useState} from 'react';
import Button from "./components/Button";
import Scene from "./components/Scene";
import AvatarScene from "./components/AvartScene";
import {Canvas} from "@react-three/fiber";
import MintNftButton from "./components/MintNftButton";


function App() {

    const [currentAccount, setCurrentAccount] = useState(null);

    const checkWalletIsConnected = async () => {
        const {ethereum} = window;

        if (!ethereum) {
            console.log("Make sure you have Metamask installed!");
            return;
        } else {
            console.log("Wallet exists! We're ready to go!")
        }

        const accounts = await ethereum.request({method: 'eth_accounts'});
        if (accounts.length !== 0) {
            const account = accounts[0];
            console.log("Found an authorized account: ", account);
            setCurrentAccount(accounts[0])
        } else {
            console.log("No authorized account found");
        }
    }

    const connectWalletHandler = async () => {
        const {ethereum} = window;

        if (!ethereum) {
            alert("Please install Metamask!");
        }

        try {
            const accounts = await ethereum.request({method: 'eth_requestAccounts'});
            console.log("Found an account! Address: ", accounts[0]);
            setCurrentAccount(accounts[0]);
        } catch (err) {
            console.log(err)
        }
    }



    useEffect(() => {
        checkWalletIsConnected();
    }, [])



    return (
        <div className='App'>
            <div className='main-app'>
                {currentAccount ?
                    (<React.Fragment>
                        <MintNftButton></MintNftButton>
                            <Canvas>
                                <Scene>
                                    <AvatarScene/>
                                </Scene>
                            </Canvas>
                        </React.Fragment>
                    )
                    : <Button title="Connect Wallet" handler={connectWalletHandler}/>}
            </div>
        </div>
    )
}

export default App;
