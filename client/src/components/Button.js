import React from "react";

const Button = props => {
    return (
        <button onClick={props.handler} className='cta-button connect-wallet-button'>
            { props.title }
        </button>
    )
}

export default Button
