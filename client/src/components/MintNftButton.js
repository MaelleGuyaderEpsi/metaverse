import React from "react";
import Web3 from "web3";
import contract from './../contracts/HouseNFT.json';
import Button from "./Button";

const MintNftButton = () => {
    const mintNftHandler = async () => {
        try {
            const {ethereum} = window;

            if (ethereum) {

                console.log(Web3.givenProvider)
                // Get network provider and web3 instance.
                const web3 = await new Web3(Web3.givenProvider || 'http://localhost:7545');

                // Use web3 to get the user's accounts.
                const accounts = await web3.eth.getAccounts();

                console.log("Network: ", await web3.eth.net.getId());
                const contractAddress = contract.networks[await web3.eth.net.getId()].address;
                const abi = contract.abi;

                // Create a contract instance
                const nftContract = new web3.eth.Contract(abi, contractAddress);
                console.log(nftContract);
                console.log("Initialize payment");

                let nftTxn = await nftContract.methods.mint().send({
                    from: accounts[0],
                    value: web3.utils.toWei("0.0001", "ether")
                }).on('receipt', function () {
                    // Placer les alert ici
                    console.log('receipt')
                });

                console.log("Mining...please wait");
                console.log("Mined: ", nftTxn.transactionHash);

            } else {
                console.log("Ethereum object does not exist");
            }

        } catch (err) {
            console.log(err);
        }
    }

    return (
        <Button title="Mint NFT" handler={mintNftHandler}/>
    )
}

export default MintNftButton
