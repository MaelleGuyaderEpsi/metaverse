import {useLoader} from "@react-three/fiber";
import {GLTFLoader} from "three/examples/jsm/loaders/GLTFLoader";
import React from "react";

const AvatarScene = () => {
    const gltf = useLoader(GLTFLoader, './models/autumn_house.glb')
    // models 3/3, model 2/2, model1/0.2
    return <primitive object={gltf.scene} scale={0.15}/>
};

export default AvatarScene
