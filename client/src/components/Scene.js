import {useLoader} from "@react-three/fiber";
import {Environment, PerspectiveCamera, OrbitControls} from "@react-three/drei";
import React, {useEffect, useRef} from "react";
import {GLTFLoader} from "three/examples/jsm/loaders/GLTFLoader";
import "./Scene.css";

const Scene = (props) => {
    const groupRef = useRef();

    const [model1, model2, model3] = useLoader(GLTFLoader, [
        "./models/autumn_house.glb",
        "./models/haunted_house.glb",
        "./models/tower_house_design.glb",
    ]);

    useEffect(() => {
        model1.scene.position.set(-10, 0, 0); // Position de la première maison
        model1.scene.scale.set(0.2, 0.2, 0.2); // Échelle de la première maison

        model2.scene.position.set(0, 0, 0); // Position de la deuxième maison
        model2.scene.scale.set(1.5, 1.5, 1.5); // Échelle de la deuxième maison

        model3.scene.position.set(10, 0, 0); // Position de la troisième maison
        model3.scene.scale.set(4, 4, 4);
    }, [model1, model2, model3]);


    return (
        <React.Fragment>
            <PerspectiveCamera makeDefault position={[0, 0, 5]}/>
            <ambientLight/>
            <directionalLight position={[10, 10, 5]} intensity={1}/>
            <group>
                {model1 && <primitive object={model1.scene}/>}
                {model2 && <primitive object={model2.scene}/>}
                {model3 && <primitive object={model3.scene}/>}
            </group>
            <OrbitControls/>
            <Environment preset="dawn" background/>
        </React.Fragment>
    );
};

export default Scene
