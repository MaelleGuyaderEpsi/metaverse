const path = require("path");

require('dotenv').config();
const PATH_PHRASE = process.env["PATH_PHRASE"];
const MUMBAI_PROJECT_ID = process.env['MUMBAI_PROJECT_ID'];

const HDWalletProvider = require('@truffle/hdwallet-provider');

console.log(MUMBAI_PROJECT_ID)
module.exports = {
    contracts_build_directory: path.join(__dirname, "client/src/contracts"),
    networks: {
        // Useful for testing. The `development` name is special - truffle uses it by default
        // if it's defined here and no other network is specified at the command line.
        // You should run a client (like ganache, geth, or parity) in a separate terminal
        // tab if you use this network and you must also set the `host`, `port` and `network_id`
        // options below to some value.
        //
        development: {
            host: "127.0.0.1",     // Localhost (default: none)
            port: 8545,            // Standard Ethereum port (default: none)
            network_id: "*"        // Any network (default: none)
        },
        Mumbai: {
            provider: () => new HDWalletProvider(PATH_PHRASE, `https://polygon-mumbai.g.alchemy.com/v2/${MUMBAI_PROJECT_ID}`),
            network_id: 80001       // Mumbai's id//
            // chain_id: 5
        }
    },
    mocha: {},
    compilers: {
        solc: {
            version: "0.8.13"
        }
    },
    db: {
        enabled: false
    }
};
